import React from 'react'
import { connect } from 'react-redux'
import { registerUser } from '../../actions/auth'

import TextInput from '../common/TextInput'

import logo from '../../media/logo.svg'

class Register extends React.Component {
  constructor(props, context) {
    super(props, context)
    
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }
  
  handleFormSubmit(formProps) {
    
  }
  
  render() {
    
    return (
      <div className="auth-layout">
        <div className="container">
          <div className="logo-holder">
            <img src={logo} alt=""/>
          </div>
          <div className="form-holder">
            <form>
              <div className="form-row">
                <div className="col-1-1">
                  <TextInput label="Email" name="email"/>
                </div>
              </div>
              <div className="form-row">
                <div className="col-1-1">
                  <TextInput label="Password" type="password" name="password"/>
                </div>
              </div>
              <div className="form-row submit-holder">
                <div className="col-1-1">
                  <button type="submit" className="btn btn-medium btn-green">
                    Register
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div className="after-form">
            Already have an account? <a href="#">Sign In</a>
          </div>
        </div>
      </div>
    )
  }
}

export default Register