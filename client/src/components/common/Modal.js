import React, { PropTypes } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

const Modal = ({isOpen, children, closeModalFn, title, subtitle}) => (
  <div>
    {isOpen === true ? (
      <ReactCSSTransitionGroup transitionName="modal-anim" transitionEnterTimeout={500} transitionLeaveTimeout={300}>
        <div className="modal-mask">
          <div className="modal-wrapper">
            <div className="modal-container">
              {title && <div className="title">{title}<small>{subtitle}</small></div>}
              {children}
            </div>
          </div>
        </div>
        <div className="modal-backdrop" onClick={closeModalFn}></div>
      </ReactCSSTransitionGroup>
    ) : (
      <ReactCSSTransitionGroup transitionName="modal-anim" transitionEnterTimeout={500} transitionLeaveTimeout={300} />
    )}
  </div>
)

Modal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  children: PropTypes.object.isRequired,
  closeModalFn: PropTypes.func.isRequired,
  title: PropTypes.string,
  subtitle: PropTypes.string
}

export default Modal