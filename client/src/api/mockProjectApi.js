// import delay from './delay'
const delay = 300

// This file mocks a web API by working with the hard-coded data below.
// It uses setTimeout to simulate the delay of an AJAX call.
// All calls return promises.
const projects = [
  {
    id: "1",
    color: "#deefff",
    projectName: "Century Genomics",
    client: "Gaumina",
    estimatedHours: "48",
    overallBudget: "-",
    startDate: "Feb 8",
    finishDate: "Feb 22",
    status: "active"
  },
  {
    id: "2",
    color: "#f4e9f7",
    projectName: "EWA",
    client: "Gaumina",
    estimatedHours: "72",
    overallBudget: "1440",
    startDate: "Feb 6",
    finishDate: "Feb 15",
    status: "active"
  },
  {
    id: "3",
    color: "#e9f7f7",
    projectName: "Logipolijos vagonai",
    client: "Gaumina",
    estimatedHours: "46",
    overallBudget: "920",
    startDate: "Feb 13",
    finishDate: "Feb 24",
    status: "potential"
  }
]

function replaceAll(str, find, replace) {
  return str.replace(new RegExp(find, 'g'), replace)
}

//This would be performed on the server in a real app. Just stubbing in.
const generateId = (project) => {
  return replaceAll(project.projectName, ' ', '-')
}

class ProjectApi {
  static getAllProjects() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(Object.assign([], projects))
      }, delay)
    })
  }

  static saveProject(project) {
    project = Object.assign({}, project) // to avoid manipulating object passed in.
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        // Simulate server-side validation
        const minProjectTitleLength = 1
        if (project.projectName.length < minProjectTitleLength) {
          reject(`Title must be at least ${minProjectTitleLength} characters.`)
        }

        if (project.id) {
          const existingProjectIndex = projects.findIndex(a => a.id == project.id)
          projects.splice(existingProjectIndex, 1, project)
        } else {
          //Just simulating creation here.
          //The server would generate ids and watchHref's for new projects in a real app.
          //Cloning so copy returned is passed by value rather than by reference.
          project.id = generateId(project)
          projects.push(project)
        }

        resolve(project)
      }, delay)
    })
  }

  static deleteProject(projectId) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        const indexOfProjectToDelete = projects.findIndex(project => {
          project.projectId == projectId
        })
        projects.splice(indexOfProjectToDelete, 1)
        resolve()
      }, delay)
    })
  }
}

export default ProjectApi