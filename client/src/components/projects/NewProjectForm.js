import React, { PropTypes } from 'react'

import TextInput from '../common/TextInput'
import Select from '../common/Select'
import Toggler from '../common/Toggler'
import ColorPicker from '../common/ColorPicker'
import Datepicker from 'react-datepicker'
import moment from 'moment'

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import * as projectActions from '../../actions/projectActions'

class NewProjectForm extends React.Component {
  constructor(props, context) {
    super(props, context)
    
    this.handleChangeStart = this.handleChangeStart.bind(this)
    this.handleChangeEnd = this.handleChangeEnd.bind(this)
    this.handleBudgetTypeChange = this.handleBudgetTypeChange.bind(this)
    this.handleColorChange = this.handleColorChange.bind(this)
    this.updateProjectState = this.updateProjectState.bind(this)
    this.handleOverallBudget = this.handleOverallBudget.bind(this)
    
    this.saveNewProject = this.saveNewProject.bind(this)
    
    this.state = {
      project: Object.assign({}, this.props.project),
      budgetType: [
        {name: 'hours', status: true},
        {name: 'money', status: false}
      ]
    }
  }
  
  updateProjectState(e) {
    const field = name || e.target.name
    let project = this.state.project
    project[field] = e.target.value
    
    if (field === 'hourlyRate' || field === 'estimatedHours') {
      this.handleOverallBudget()
    }
    
    return this.setState({project})
  }
  
  handleOverallBudget() {
    const field = document.querySelector('[name="overallBudget"]')
    const hourlyRate = this.state.project.hourlyRate
    const estimatedHours = this.state.project.estimatedHours
    const newValue = (hourlyRate * estimatedHours > 0) ? hourlyRate * estimatedHours : ''
    
    field.value = newValue
    this.setState({project: {overallBudget: newValue}})
  }
  
  handleChangeStart(date) {
    const field = 'startDate'
    let project = this.state.project
    project[field] = date
    return this.setState({project})
  }
  
  handleChangeEnd(date) {
    const field = 'finishDate'
    let project = this.state.project
    project[field] = date
    return this.setState({project})
  }
  
  handleBudgetTypeChange(e) {
    this.setState({
      budgetType: [
        {name: 'hours', status: e.target.checked ? false : true},
        {name: 'money', status: e.target.checked ? true : false}
      ]
    })
  }
  
  handleColorChange(color) {
    this.setState({ project: {color} })
  }
  
  saveNewProject(e) {
    e.preventDefault()
    this.props.actions.saveProject(this.state.project)
  }
  
  render() {
    
    return (
      <div className="new-project-form">
        <form>
          <div className="form-row">
            <div className="col-3-4">
              <TextInput
                name="projectName"
                label="Name"
                placeholder=""
                onChange={this.updateProjectState}
              />
            </div>
            <div className="col-1-4">
              <ColorPicker
                label="Color"
                name="projectColor"
                onChange={this.handleColorChange}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-1-2">
              <Select
                name="client"
                label="Client"
              />
            </div>
            <div className="col-1-2">
              <div className="create-new-client-btn">or <span>Create a new Client</span></div>
            </div>
          </div>
          <hr />
          <div className="form-row">
            <div className="col-1-2">
              <div className="form-item type-text">
                <label>Starts on</label>
                <Datepicker 
                  name="startDate"
                  dateFormat="YYYY-MM-DD"
                  placeholderText="YYYY-MM-DD"
                  selected={this.state.project.startDate}
                  onChange={this.handleChangeStart}
                  selectsStart
                  startDate={this.state.project.startDate}
                  />
              </div>
            </div>
            <div className="col-1-2">
              <div className="form-item type-text">
                <label>Finishes on</label>
                <Datepicker 
                  name="finishDate"
                  dateFormat="YYYY-MM-DD"
                  placeholderText="YYYY-MM-DD"
                  selected={this.state.project.finishDate}
                  onChange={this.handleChangeEnd}
                  selectsEnd
                  startDate={this.state.project.startDate}
                  />
              </div>
            </div>
          </div>
          <hr/>
          <div className="form-row">
            <div className="col-1-3">
              <TextInput
                name="hourlyRate"
                label="Hourly rate"
                placeholder=""
                className="icon-money"
                onChange={this.updateProjectState}
              />
            </div>
            <div className="col-1-3">
              <TextInput
                name="estimatedHours"
                label="Estimated hours"
                placeholder=""
                className="icon-time"
                onChange={this.updateProjectState}
              />
            </div>
            <div className="col-1-3">
              <TextInput
                name="overallBudget"
                label="Overall budget"
                placeholder=""
                className="icon-money"
                onChange={this.updateProjectState}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-1-1 form-footer">
              <div className="btn-cancel">
                Cancel
              </div>
              <button type="submit" className="btn btn-medium btn-green" onClick={this.saveNewProject}>
                Create New Project
              </button>
            </div>
          </div>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  let project = {
    projectName: '',
    color: '',
    client: '',
    startDate: '',
    finishDate: '',
    hourlyRate: '',
    estimatedHours: '',
    overallBudget: '',
    status: 'active'
  }
  
  return { project }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(projectActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewProjectForm)