import React, { PropTypes } from 'react'

const Select = ({name, label}) => (
  <div className="form-item type-select">
    <label>{label}</label>
    <select name="{name}">
      <option value="1">Option #1</option>
      <option value="2">Option #2</option>
    </select>
  </div>
)

Select.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string
}

export default Select