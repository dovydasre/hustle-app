import React, { Component, PropTypes } from 'react';
import Header from './common/Header'

class App extends Component {
  render() {
    
    const location = this.props.location.pathname
    const renderHeader = location === '/register' ? false : true
    
    return (
      <div>
        {renderHeader && <Header />}
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired
}

export default App;