import React, { PropTypes } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as projectActions from '../../actions/projectActions'

import ProjectsTable from './ProjectsTable'
import Modal from '../common/Modal'
import NewProjectForm from './NewProjectForm'

class Projects extends React.Component {
  constructor(props, context) {
    super(props, context)
    
    this.showModal = this.showModal.bind(this)
    this.closeModal = this.closeModal.bind(this)
    this.state = {
      isOpen: false,
    }
  }
  
  showModal() {
    this.setState({isOpen: true})
  }
  
  closeModal() {
    this.setState({isOpen: false})
  }
  
  render() {
    const { projects } = this.props
    const activeProjects = projects.filter(p => p.status === 'active')
    const potentialProjects = projects.filter(p => p.status === 'potential')
    const completedProjects = projects.filter(p => p.status === 'completed')
    
    return (
      <div>
        <div className="container">
          <ProjectsTable projects={activeProjects} title="Active" showCreateNewBtn={true} showModalFn={this.showModal} />
          <ProjectsTable projects={potentialProjects} title="Potential" />
          <ProjectsTable projects={completedProjects} title="Completed" />
        </div>
        <Modal isOpen={this.state.isOpen} closeModalFn={this.closeModal} title="New Project" subtitle="Awesome! Let’s create your project">
          <NewProjectForm />
        </Modal>
      </div>
    )
  }
}

function mapStateToProps(state, ownProps) {
  let project = {}
  
  return {
    projects: state.projects,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(projectActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Projects)