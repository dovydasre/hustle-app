import React, { PropTypes } from 'react'

const Toggler = ({name, label, options, onChange}) => (
  <div className="form-item type-toggler">
    <div className="label">{label}</div>
    <label>
      <input type="checkbox" onChange={onChange}/>
      <div>
        <div className="box"></div>
        <div className="text">
          {options.map(o =>
            <div key={o.name}>{o.name}</div>
          )}
        </div>
      </div>
    </label>
  </div>
)

Toggler.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  options: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired
}

export default Toggler