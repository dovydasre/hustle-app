import * as types from '../actions/actionTypes'

export default function projectReducer(state = [], action) {
  switch (action.type) {
    case types.LOAD_PROJECTS_SUCCESS:
      return action.projects
    case types.CREATE_PROJECTS_SUCCESS:
      console.log([...state, Object.assign({}, action.project)]);
      return [...state, Object.assign({}, action.project)]
    case types.UPADATE_PROJECTS_SUCCESS:
      return [...state.filter(project => project.id !== action.project.id), Object.assign({}, action.project)]
    default:
      return state  
  }
}
