import React, { PropTypes } from 'react'

const ProjectsTable = ({ projects, title, showCreateNewBtn, showModalFn }) => (
  <div className="projects-table-layout">
    <div className="table-heading">
      <div className="title">{title} <span>{projects.length}</span></div>
      {showCreateNewBtn && <button className="btn btn-small" onClick={showModalFn}>+ Add new</button>}
    </div>
    {projects.length ? (
      <table className="default-style">
        <thead>
          <tr>
            <th>Project name</th>
            <th>Client</th>
            <th>Time Budget</th>
            <th>Money Budget</th>
            <th>Start</th>
            <th>Finish</th>
          </tr>
        </thead>
        <tbody>
          {projects.map(p => 
            <tr key={p.id}>
              <td>
                <div className="project-color" style={{backgroundColor: p.color}}></div>
                {p.projectName}
              </td>
              <td>{p.client}</td>
              <td>{p.timeBudged}</td>
              <td>{p.moneyBudged}</td>
              <td>{p.start}</td>
              <td>{p.finish}</td>
            </tr>
          )}
        </tbody>
      </table>
    ) : (
      <div className="no-projects">No <span>{title}</span> projects.</div>
    )}
  
  </div>
)

ProjectsTable.propTypes = {
  projects: PropTypes.array.isRequired,
  title: PropTypes.string,
  showCreateNewBtn: PropTypes.bool,
  showModalFn: PropTypes.func
}

export default ProjectsTable