import React, { PropTypes } from 'react'
import onClickOutside from 'react-onclickoutside'

class ColorPicker extends React.Component {
  constructor(props, context) {
    super()
    
    this.getRandomColor = this.getRandomColor.bind(this)
    this.handleColorPicked = this.handleColorPicked.bind(this)
    this.toggleVisibility = this.toggleVisibility.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    
    this.state = {
      activeColor: this.getRandomColor(),
      isOpen: false
    }
  }
  
  getRandomColor() {
    return '#' + ((1 << 24) * Math.random() | 0).toString(16)
  }
  
  handleColorPicked(selecteColor) {
    if (selecteColor === 'random') {
      this.setState({ activeColor: this.getRandomColor() })
    } else {
      this.setState({ activeColor: selecteColor, isOpen: false })
    }
    
    this.props.onChange(this.state.activeColor)
  }
  
  toggleVisibility() {
    let newVal = this.state.isOpen ? false : true
    this.setState({ isOpen:  newVal })
  }
  
  handleClickOutside() {
    this.setState({ isOpen:  false })
  }
  
  render() {
    let colors = ['#ff3d4c', '#80ddbe', '#00968f', '#fbe100', '#3498db', '#7c6fff', '#afe2d3', '#ff8d8d']
    
    return (
      <div className="form-item type-color-picker">
      <label>{this.props.label}</label>
        <div className="component-holder">
          <div className={"input " + (this.state.isOpen ? 'open' : '')} onClick={this.toggleVisibility}>
            <div className="color-box" style={{ backgroundColor: this.state.activeColor }}></div>
          </div>
          <div className="colors-dropdown">
            <div className="colors-list">
              {colors.map(c =>
                <div
                  key={c}
                  style={{ backgroundColor: c }}
                  onClick={() => this.handleColorPicked(c)}
                >
                </div>
              )}
              <div className="shuffle" onClick={() => this.handleColorPicked('random')}>
                <i className="icon-repeat"></i>
              </div>
            </div>
          </div>
        </div>
        <input type="text" value={this.state.activeColor} onChange={this.props.onChange}/>
      </div>
    )
  }
}

ColorPicker.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

export default onClickOutside(ColorPicker)