import React from 'react'
import { Route, IndexRoute } from 'react-router'
import App from './components/App'
import Projects from './components/projects/Projects'
import Clients from './components/clients/Clients'

import Register from './components/auth/Register'

export default (
  <Route path="/" component={App}>
    <IndexRoute component={Projects} />
    <Route path="projects" component={Projects} />
    <Route path="clients" component={Clients} />
    
    <Route path="register" component={Register} />
      
  </Route>
)