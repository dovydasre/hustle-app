import React, { PropTypes } from 'react'

const TextInput = ({name, label, type = 'text', placeholder, className, onChange}) => (
  <div className={"form-item type-text " + className}>
    {label && <label>{label}</label>}
    <input type={type} name={name} placeholder={placeholder} onChange={onChange}/>
  </div>
)

TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  type: PropTypes.string,
  placeholder: PropTypes.string,
  className: PropTypes.string,
  onChange: PropTypes.func.isRequired
}

export default TextInput