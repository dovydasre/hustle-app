import React from 'react'
import { render } from 'react-dom'
import configureStore from './store/configureStore'
import { Provider } from 'react-redux'
import { Router, browserHistory } from 'react-router'
import routes from './routes'
import { loadProjects } from './actions/projectActions'
import App from './components/App'

import fonts from './media/fonts/fonts.scss'
import styles from './styles/main.scss'

const store = configureStore()
store.dispatch(loadProjects())

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('root')
)