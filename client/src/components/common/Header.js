import React from 'react'
import { Link, IndexLink } from 'react-router'
import logo from '../../media/logo.svg'

const Header = () => (
  <header>
    <div>
      <Link to="/projects">
        <img src={logo} alt=""/>
      </Link>
    </div>
    <nav>
      <a href="#">Dashboard</a>
      <a href="#">Schedule</a>
      <Link to="/projects" activeClassName="active">Projects</Link>
      <Link to="/clients" activeClassName="active">Clients</Link>
      <a href="#">Expenses</a>
    </nav>
    <div>
      <div className="avatar"></div>
    </div>
  </header>
)

export default Header