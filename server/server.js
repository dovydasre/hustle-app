// BASE SETUP
// =============================================================================
const express      = require('express');
const app          = express();
const bodyParser   = require('body-parser');
const morgan       = require('morgan');
const mongoose     = require('mongoose');

const router = require('./router');  

const jwt    = require('jsonwebtoken');
const config = require('./config');
const User   = require('./models/user');


// CONFIGURATION
// =============================================================================
const port = process.env.PORT || 3001;
mongoose.connect(config.database);

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));

// Enable CORS from client-side
app.use(function(req, res, next) {  
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");
  res.header("Access-Control-Allow-Credentials", "true");
  next();
});

// START THE SERVER
// =============================================================================
app.listen(port, () => console.log('Magic happens at http://localhost:' + port));

router(app);